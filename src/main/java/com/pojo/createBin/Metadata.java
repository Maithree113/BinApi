
package com.pojo.createBin;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({
    "id",
    "createdAt",
    "private",
    "name",
    "collectionId"
})
public class Metadata {

    private String id;
    private String createdAt;
    private Boolean _private;
    private String name;
    private String collectionId;
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    public Boolean getPrivate() {
        return _private;
    }
    public String getName() {
        return name;
    }

    public String getCollectionId() {
        return collectionId;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId;
    }
    public void setPrivate(Boolean _private) {
        this._private = _private;
    }
}
