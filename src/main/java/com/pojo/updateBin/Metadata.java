package com.pojo.updateBin;

public class Metadata {

    private Integer version;
    private String parentId;
    private Boolean _private;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Boolean getPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }
    public Integer getVersion() { return version; }

    public void setVersion(Integer version) { this.version = version; }
}
