package com.pojo.deleteBin;

public class Metadata {
    private String id;
    private Integer versionsDeleted;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public Integer getVersionsDeleted() { return versionsDeleted; }

    public void setVersionsDeleted(Integer versionsDeleted) { this.versionsDeleted = versionsDeleted; }
}
