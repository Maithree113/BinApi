package com.pojo.deleteBin;

public class DeleteBinResponse {
    private Metadata metadata;
    private String message;

    public Metadata getMetadata() { return metadata; }

    public void setMetadata(Metadata metadata) { this.metadata = metadata; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
