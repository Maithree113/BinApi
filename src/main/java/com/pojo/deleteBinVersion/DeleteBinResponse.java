package com.pojo.deleteBinVersion;

public class DeleteBinResponse {
    private MetaData metaData;
    private String message;

    public MetaData getMetaData() { return metaData; }

    public void setMetaData(MetaData metaData) { this.metaData = metaData; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }
}
