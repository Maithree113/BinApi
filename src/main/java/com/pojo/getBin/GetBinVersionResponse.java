package com.pojo.getBin;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetBinVersionResponse {
    private MetaData metaData;
    @JsonProperty("metaData")
    public MetaData getMetaData() { return metaData; }

    public void setMetaData(MetaData metaData) { this.metaData = metaData; }
}