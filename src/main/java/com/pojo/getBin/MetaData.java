package com.pojo.getBin;

public class MetaData {

    private Integer versionCount;
    private String id;
    private Boolean _private;

    public String getId() {
        return id;
    }

    public void setId(String parentId) {
        this.id = parentId;
    }

    public Boolean getPrivate() {
        return _private;
    }

    public void setPrivate(Boolean _private) {
        this._private = _private;
    }
    public Integer getVersionCount() { return versionCount; }

    public void setVersionCount(Integer version) { this.versionCount = version; }
}
