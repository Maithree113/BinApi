Feature: Validate User can create a new bin using create bin api

  @CreateBinUsingCollection
  Scenario Outline: Verify user can create a public/private bin using collection
    Given I am an authorized user with permission to create bin with access to collection
    When I try to create bin with collection which is public "<value>"
    Then bin should be created with public access "<value>"

    Examples:
      |value |
      |true  |
      |false |

  @CreateBinWithoutCollection
  Scenario Outline: Verify user can create bin without collection
    Given I am an authorized user with permission to create bin with access to collection
    When I try to create bin
    Then bin should be created with public access "<value>"

    Examples:
      |value |
      |true  |

  @CreateEmptyBin
  Scenario Outline: Verify user cannot create an empty bin
    GivenI am an authorized user with permission to create bin with access to collection
    When I try to create an empty bin
    Then I should not be able to create bin and proper error message with status <status> should be displayed "<message>"
    Examples:
      |status  |message              |
      |400     | Bin cannot be blank |

  @CreateBinWithInvalidCollection
  Scenario Outline: Verify user cannot create a bin with invalid collection
    Given I am an authorized user with permission to create bin with access to collection
    When I try to create bin with collection "<collectionName>" which is public "<value>"
    Then I should not be able to create bin and proper error message with status <status> should be displayed "<message>"

    Examples:
      | collectionName |value |status  |message                         |
      | myBin          |true  |400     |Invalid X-Collection-Id provided|

  @CreateBinWithLongBinName
  Scenario Outline: Verify user cannot create a bin with a long bin name
    Given I am an authorized user with permission to create bin with access to collection
    When I try to create bin with bigName with public access "<value>"
    Then I should not be able to create bin and proper error message with status <status> should be displayed "<message>"
    Examples:
      |status  |message                                          |value  |
      |400     |X-Bin-Name cannot be blank or over 128 characters| true  |
