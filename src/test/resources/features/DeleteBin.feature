Feature: Validate User can delete a bin

  @DeleteBin
  Scenario Outline: Verify user can delete a bin
    Given I am an authorized user with permission to delete bin created by me
    When I try to delete bin with id "<binId>"
    Then bin should be deleted with status 200 and message "<message>"
    Examples:
      | binId  |message                 |
      | setId  |Bin deleted successfully|

  @DeleteBinWithInvalidId
  Scenario Outline: Verify user cannot delete a bin with invalid bin id
    Given I try to delete bin with id "<binId>"
    Then bin should not be deleted and proper error message with status <status> should be displayed "<message>"
    Examples:
      | binId                   |message                                            |status|
      | 60c49d42a8bf076b5f65c6ae| Bin not found or it doesn't belong to your account| 404  |

  @DeleteAllBinVersions
  Scenario Outline: Verify user can delete all versions of a bin
    Given I am an authorized user with permission to delete bin created by me
    When I try to delete bin with all of its version set to "<preserveLatest>"
    Then bin deleted with status 200 and message "<message>"
    Examples:
      | preserveLatest |message          |
      | true           |No versions found|