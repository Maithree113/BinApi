Feature: Create,Update,Get,Delete a bin

  @E2E
  Scenario Outline: Verify user can create,update,get and delete a public bin using collection
    Given I am an authorized user with permission to create bin with access to collection
    When I try to create bin with collection which is public "<value>"
    Then bin should be created with public access "<value>"
    When I try to update bin "<binId>" with versioning set to "<isVersioning>"
    Then bin should be updated
    When I try to get bin "<binId>" details with versioning set to "<versionValue>"
    Then bin details should be displayed
    When I try to get bin "<binId>" version count details
    Then bin details with version count should be displayed
    When I try to delete bin with all of its version set to "<preserveLatest>"
    Then bin should get deleted with status 200 and message "<message>"

    Examples:
      |value  |binId  |preserveLatest|versionValue|message                                                                                        |isVersioning|
      |false  |setId  |true          |  1         |Versions for the Bin are deleted successfully and latest version preserved on the base record. |true        |

