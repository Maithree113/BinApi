Feature: Validate User can update details of an existing bin

  @UpdateBinWithValidBinId
  Scenario Outline: Verify user can update a public/private bin and set versioning value
    Given I am an authorized user with permission to update bin created by me
    When I try to update bin "<binId>" with versioning set to "<value>"
    Then bin should be updated

    Examples:
     |binId |value |
     |setId |false |

  @UpdateBinWithInValidBinId
  Scenario Outline: Verify user cannot update an invalid bin
    Given I am an authorized user with permission to update bin created by me
    When I try to update bin "<binId>" with versioning set to "<value>"
    Then bin should not be updated and proper error message with status <status> should be displayed "<message>"

    Examples:
      |binId                    |value |message       |status|
      | 60c49d42a8bf076b5f65c6ae|true  |Bin not found | 404  |

  @UpdateBinNoData
  Scenario Outline: Verify user cannot update a bin without any data
    Given I am an authorized user with permission to update bin created by me
    When I try to update bin
    Then bin should not be updated and proper error message with status <status> should be displayed "<message>"

    Examples:
     |message            |status|
     |Bin cannot be blank|400   |




