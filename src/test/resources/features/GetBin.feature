Feature: Validate User can see bin details

  @GetBinDetailsWithValidBinId
  Scenario Outline: Verify user can get bin details based on bin id
    Given I am an authorized user with permission to see bin details created using my collection
    When I try to get bin "<binId>" details with versioning set to "<value>"
    Then bin details should be displayed

    Examples:
      |binId |value |
      |setId |latest|

  @GetBinDetailsWithInValidBinId
  Scenario Outline: Verify user cannot get bin details when invalid bin id is provided
    Given I am an authorized user with permission to see bin details created using my collection
    When I try to get bin "<binId>" details with versioning set to "<value>"
    Then bin details should not be displayed and proper error message with status 404 should be displayed "<message>"
    And I try to get bin "<binId>" version count details
    Then bin details should not be displayed and proper error message with status 404 should be displayed "<message>"

    Examples:
      |binId                   |value |message|
      |60c49d42a8bf076b5f65c6ae|latest|Bin not found or it doesn't belong to your account|

  @GetBinVersionDetailsWithValidId
  Scenario Outline: Verify user can get bin version count based on bin Id
    Given I am an authorized user with permission to see bin details created using my collection
    When I try to get bin "<binId>" version count details
    Then bin details with version count should be displayed

    Examples:
      |binId  |
      |setId  |

