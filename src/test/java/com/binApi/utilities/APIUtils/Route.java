package com.binApi.utilities.APIUtils;

public class Route {

    private static final String BIN_ROUTE = "/b";

    public static String createBin() { return BIN_ROUTE; }

    public static String updateBin(String binId) {
        return BIN_ROUTE + "/" + binId;
    }

    public static String getBin(String binId, String version) { return BIN_ROUTE + "/" + binId + "/" + version; }

    public static String getBinWithVersionCount(String binId) { return BIN_ROUTE + "/" + binId + "/versions/count"; }

    public static String deleteBin(String binId) { return BIN_ROUTE + "/" + binId; }

    public static String deleteAllBin(String binId) { return BIN_ROUTE + "/" + binId + "/versions"; }

}
