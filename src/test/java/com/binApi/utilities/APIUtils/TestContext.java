package com.binApi.utilities.APIUtils;

import com.binApi.dataProvider.ConfigReader;

public class TestContext {

    private ScenarioContext scenarioContext;

    private EndPoints endPoints;

    public TestContext() {

        endPoints = new EndPoints(ConfigReader.getInstance().getBaseUrl(), ConfigReader.getInstance().getMasterKey());
        scenarioContext = new ScenarioContext();
    }

    public EndPoints getEndPoints() {
        return endPoints;
    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }

}
