package com.binApi.utilities.APIUtils;

import com.pojo.createBin.CreateBinRequest;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class EndPoints {

    private final RequestSpecification request;

    public EndPoints(String baseUrl,String masterKey)
    {
        RestAssured.baseURI = baseUrl;
        request = given()
                .header("X-Master-Key", masterKey)
                .contentType(ContentType.JSON);
    }

    public Response createBin(String access, String collectionName, String binName, CreateBinRequest binBody) {
        return request.
                header("X-Bin-Private", access)
                .header("X-Collection-Id", collectionName)
                .header("X-Bin-Name", binName)
                .body(binBody)
                .post(Route.createBin())
                .then().extract().response();
    }

    public Response createBin(String collectionName, String binName) {
        return request.
                 header("X-Collection-Id", collectionName)
                .header("X-Bin-Name", binName)
                .post(Route.createBin())
                .then().extract().response();
    }

    public Response updateBin(String binId, String binVersion, CreateBinRequest updateBinBody) {
       return request.
                 header("X-Bin-Versioning", binVersion)
                .body(updateBinBody)
                .put(Route.updateBin(binId))
                .then().extract().response();
    }

    public Response updateBin(String binId) {
        return request.
                put(Route.updateBin(binId))
                .then().extract().response();
    }

    public Response deleteBin(String binId) {
        return request.
                    delete(Route.deleteBin(binId))
                    .then().extract().response();

    }

    public Response getBin(String binId, String binVersion) {
       return request.
                 get(Route.getBin(binId, binVersion))
                .then().extract().response();
    }

    public Response getBinWithVersionCount(String binId) {
        return request.
                get(Route.getBinWithVersionCount(binId))
                .then().extract().response();
    }

    public Response deleteAllBin(String binId, String preserveLatest) {
        return request.
                header("X-Preserve-Latest", preserveLatest)
                .delete(Route.deleteAllBin(binId))
                .then().extract().response();
    }

}
