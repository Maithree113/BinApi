package com.binApi.dataProvider;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigReader {
    private Properties properties;
    private static ConfigReader configReader;
    private ConfigReader() {
        BufferedReader reader;
        String propertyFilePath = "src/test/java/com/binApi/config/configurations.properties";
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public static ConfigReader getInstance( ) {
        if(configReader == null) {
            configReader = new ConfigReader();
        }
        return configReader;
    }

    public String getBaseUrl() {
        String baseUrl = properties.getProperty("baseUrl");
        if(baseUrl != null) return baseUrl;
        else throw new RuntimeException("base_Url not specified in the Configuration.properties file.");
    }

    public String getMasterKey() {
        String masterKey = properties.getProperty("masterKey");
        if(masterKey != null) return masterKey;
        else throw new RuntimeException("masterKey not specified in the Configuration.properties file.");
    }

    public String getCollectionId() {
        String collectionId = properties.getProperty("collectionId");
        if(collectionId != null) return collectionId;
        else throw new RuntimeException("collectionId not specified in the Configuration.properties file.");
    }
}
