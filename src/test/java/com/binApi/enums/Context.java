package com.binApi.enums;

public enum Context {
    PUBLIC_BIN_ID,
    PRIVATE_BIN_ID,
    COLLECTION_ID,
    ID
}
