package com.binApi.stepdefinations;

import com.binApi.dataProvider.ConfigReader;
import com.binApi.enums.Context;
import com.binApi.utilities.APIUtils.TestContext;
import com.pojo.createBin.CreateBinRequest;
import com.pojo.createBin.CreateBinResponse;
import com.pojo.createBin.ErrorMessage;
import com.pojo.deleteBin.DeleteBinResponse;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class DeleteBinSteps extends BaseStep {

    public DeleteBinSteps(TestContext testContext) {
        super(testContext);
    }

    @Before
    public void beforeStep(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("I am an authorized user with permission to delete bin created by me")
    public void i_am_an_authorized_user_with_permission_to_delete_bin() {
        if(collectionId == null) {
            getScenarioContext().setContext(Context.COLLECTION_ID, ConfigReader.getInstance().getCollectionId());
            collectionId = String.valueOf(getScenarioContext().getContext(Context.COLLECTION_ID));
        }
        if (binId == null || binId.equals("null")) {
            response = getEndPoints().createBin(value, collectionId, binName, new CreateBinRequest(requestBody));
            binId = response.getBody().as(CreateBinResponse.class).getMetadata().getId();
        }
    }

    @When("I try to delete bin with id {string}")
    public void i_try_to_delete_bin_with_id(String id) {
      if(id.equals("setId")) { response = getEndPoints().deleteBin(binId); }
      else {
          getScenarioContext().setContext(Context.ID, id);
          response = getEndPoints().deleteBin(id); }
        scenario.log("Delete API is called to delete bin");
    }

    @Then("bin should be deleted with status {int} and message {string}")
    public void bin_should_be_deleted(int status, String message) {
        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(DeleteBinResponse.class).getMetadata().getId(), binId);
        Assert.assertEquals(response.getBody().as(DeleteBinResponse.class).getMessage(), message);
        scenario.log("Bin with id " + binId + " is successfully deleted");
        binId = String.valueOf(getScenarioContext().getContext(Context.PRIVATE_BIN_ID));
    }

    @Then("bin should not be deleted and proper error message with status {int} should be displayed {string}")
    public void bin_should_not_be_deleted_and_proper_error_message_with_status(int status, String message) {
        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(ErrorMessage.class).getMessage(), message);
        scenario.log("Bin with id " + getScenarioContext().getContext(Context.ID) + " could not be deleted because of " + message);
    }

    @When("I try to delete bin with all of its version set to {string}")
    public void iTryToDeleteBinWithAllOfItsVersionSetTo(String preserveLatest) {
        response = getEndPoints().deleteAllBin(binId, preserveLatest);
        scenario.log("Trying to delete Bin with id " + binId + " with preserve latest set to " + preserveLatest);
    }

    @Then("bin deleted with status {int} and message {string}")
    public void binDeletedWithStatusAndMessage(int status, String message) {
        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(ErrorMessage.class).getMessage(), message);
        scenario.log("Bin with id " + binId + " is successfully deleted with " + message);
        binId = String.valueOf(getScenarioContext().getContext(Context.PRIVATE_BIN_ID));
    }
}
