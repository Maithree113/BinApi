package com.binApi.stepdefinations;

import com.binApi.dataProvider.ConfigReader;
import com.binApi.enums.Context;
import com.binApi.utilities.APIUtils.TestContext;
import com.pojo.createBin.CreateBinRequest;
import com.pojo.createBin.CreateBinResponse;
import com.pojo.createBin.ErrorMessage;
import com.pojo.getBin.GetBinVersionResponse;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class GetBinSteps extends BaseStep {

    public GetBinSteps(TestContext testContext) {
        super(testContext);
    }

    @Before
    public void beforeStep(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("I am an authorized user with permission to see bin details created using my collection")
    public void i_am_an_authorized_user_with_permission_to_see_bin_details() {
        if(collectionId == null) {
            getScenarioContext().setContext(Context.COLLECTION_ID, ConfigReader.getInstance().getCollectionId());
            collectionId = String.valueOf(getScenarioContext().getContext(Context.COLLECTION_ID));
        }
        if(binId == null) {
            response = getEndPoints().createBin(value, collectionId, binName, new CreateBinRequest(requestBody));
            binId = response.getBody().as(CreateBinResponse.class).getMetadata().getId();
        }
        scenario.log("Bin is not created if there is a bin already exist");
    }

    @When("I try to get bin {string} details with versioning set to {string}")
    public void i_try_to_get_bin_details_with_versioning_set_to(String id, String version) {
        if(id.equals("setId")){ response = getEndPoints().getBin(binId, version);}
        else{
            getScenarioContext().setContext(Context.ID, id);
            response = getEndPoints().getBin(id, version);
        }
        scenario.log("GET API is called to read bin details");
    }

    @Then("bin details should be displayed")
    public void bin_details_should_be_displayed() {
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.getBody().as(CreateBinResponse.class).getMetadata().getId(), binId);
        Assert.assertEquals(response.getBody().as(CreateBinResponse.class).getMetadata().getCollectionId(), collectionId);
        scenario.log("Bin with id " + binId + " with collection id " + collectionId +" retrieved");
    }

    @When("I try to get bin {string} version count details")
    public void iTryToGetBinVersionCountDetails(String id) {
        if(id.equals("setId")){ response = getEndPoints().getBinWithVersionCount(binId);}
        else{
            getScenarioContext().setContext(Context.ID, id);
            response = getEndPoints().getBinWithVersionCount(id);
        }
        scenario.log("GET API is called to get bin version count");
    }

    @Then("bin details with version count should be displayed")
    public void binDetailsWithVersionCountShouldBeDisplayed() {
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.getBody().as(GetBinVersionResponse.class).getMetaData().getId(), binId);
        scenario.log("Bin with id " + binId + " as " + response.getBody().as(GetBinVersionResponse.class).getMetaData().getVersionCount() + " versions");
    }

    @Then("bin details should not be displayed and proper error message with status {int} should be displayed {string}")
    public void binDetailsShouldNotBeDisplayedAndProperErrorMessageWithStatusShouldBeDisplayed(int status, String message) {
        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(ErrorMessage.class).getMessage(), message);
        scenario.log("Bin with id " + getScenarioContext().getContext(Context.ID) + " could not be retrieved because " + message);
    }
}
