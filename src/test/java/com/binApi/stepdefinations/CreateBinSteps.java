package com.binApi.stepdefinations;

import com.binApi.dataProvider.ConfigReader;
import com.binApi.enums.Context;
import com.binApi.utilities.APIUtils.TestContext;
import com.pojo.createBin.CreateBinRequest;
import com.pojo.createBin.CreateBinResponse;
import com.pojo.createBin.ErrorMessage;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;

public class CreateBinSteps extends BaseStep {

    public CreateBinSteps(TestContext testContext) {
        super(testContext);
    }

    @Before
    public void beforeStep(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("I am an authorized user with permission to create bin with access to collection")
    public void i_am_an_authorized_user_with_all_permission_on_bin() {
        getScenarioContext().setContext(Context.COLLECTION_ID, ConfigReader.getInstance().getCollectionId());
        collectionId = String.valueOf(getScenarioContext().getContext(Context.COLLECTION_ID));
        scenario.log("Collection Name is : " + collectionId);
    }

    @When("I try to create bin with collection {string} which is public {string}")
    public void i_try_to_create_bin_with_collection_which_is_public(String collectionName, String access) {

        response = getEndPoints().createBin(access, collectionName, binName,
                new CreateBinRequest(requestBody));
        scenario.log("Created a new bin with collection name : "+ collectionName + " with access " + access);
    }

    @Then("bin should be created with public access {string}")
    public void bin_should_be_created_with_public_access(String expectedValue) {

        Assert.assertEquals(response.getBody().as(CreateBinResponse.class).getMetadata().getPrivate().toString(), expectedValue);
        Assert.assertEquals(response.getStatusCode(), 200);
        scenario.log("Successfully created bin");
    }

    @When("I try to create bin")
    public void i_try_to_create_bin() {
        response = getEndPoints().createBin(value, "", binName,
                new CreateBinRequest(requestBody));
        scenario.log("POST API is called to create bin");
    }

    @Then("I should not be able to create bin and proper error message with status {int} should be displayed {string}")
    public void i_should_not_be_able_to_create_bin_and_proper_error_message_with_status_should_be_displayed(int status, String message) {

        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(ErrorMessage.class).getMessage(), message);
        scenario.log("Bin was not created because " + message);
    }

    @When("I try to create an empty bin")
    public void i_try_to_create_an_empty_bin() {

        response =  getEndPoints().createBin(binCollection, binName);
        scenario.log("POST API is called to create bin");
    }

    @When("I try to create bin with collection which is public {string}")
    public void iTryToCreateBinWithCollectionWhichIsPublic(String access) {
        response = getEndPoints().createBin(access ,collectionId, binName,
                new CreateBinRequest(requestBody));

        binId = response.getBody().as(CreateBinResponse.class).getMetadata().getId();

        if(access.equals("true")) {
            getScenarioContext().setContext(Context.PRIVATE_BIN_ID, binId);
            scenario.log("Bin created with " + binId + " with public access true");
        }
        else{
            getScenarioContext().setContext(Context.PUBLIC_BIN_ID, binId);
            scenario.log("Bin created with " + binId + " with public access false");
        }
    }

    @When("I try to create bin with bigName with public access {string}")
    public void iTryToCreateBinWithBigNameWithPublicAccess(String access) {
        String binName = RandomStringUtils.randomNumeric(150);
        response = getEndPoints().createBin(access,getScenarioContext().getContext(Context.COLLECTION_ID).toString(),
                binName, new CreateBinRequest(requestBody));
        scenario.log("Trying to create bin with bin Name " + binName + " with public access false");
    }
}
