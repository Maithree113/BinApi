package com.binApi.stepdefinations;
import com.binApi.dataProvider.ConfigReader;
import com.binApi.enums.Context;
import com.binApi.utilities.APIUtils.TestContext;
import com.pojo.createBin.CreateBinRequest;
import com.pojo.createBin.CreateBinResponse;
import com.pojo.createBin.ErrorMessage;
import com.pojo.updateBin.UpdateBinResponse;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class UpdateBinSteps extends BaseStep {

    public UpdateBinSteps(TestContext testContext) {
        super(testContext);
    }

    @Before
    public void beforeStep(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("I am an authorized user with permission to update bin created by me")
    public void i_am_an_authorized_user_with_permission_to_update_bin() {
        if(collectionId == null) {
            getScenarioContext().setContext(Context.COLLECTION_ID, ConfigReader.getInstance().getCollectionId());
            collectionId = String.valueOf(getScenarioContext().getContext(Context.COLLECTION_ID));
        }

        if(binId == null) {
            response = getEndPoints().createBin(value, getScenarioContext().getContext(Context.COLLECTION_ID).toString(),
                    binName, new CreateBinRequest(requestBody));
            binId = response.getBody().as(CreateBinResponse.class).getMetadata().getId();
        }
        scenario.log("Bin is not created if there is a bin already exist");
    }

    @When("I try to update bin {string} with versioning set to {string}")
    public void i_try_to_update_bin_with_versioning_set_to(String id, String version) {
        if(id.equals("setId")){ response = getEndPoints().updateBin(binId, version, new CreateBinRequest(updateRequestBody));
            scenario.log("PUT API is called to get update bin details");}
        else{
            getScenarioContext().setContext(Context.ID, id);
            response = getEndPoints().updateBin(id, version, new CreateBinRequest(updateRequestBody)); }
            scenario.log("Updating bin with id " + binId + " with " +updateRequestBody + " with version " +version);
    }

    @Then("bin should be updated")
    public void bin_should_be_updated() {
        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertEquals(response.getBody().as(UpdateBinResponse.class).getMetadata().getParentId(), binId);
        Assert.assertEquals(response.getBody().as(UpdateBinResponse.class).getRecord().getSample(), updateRequestBody);
        scenario.log("Bin with id " + binId + " is updated with " + updateRequestBody);
    }

    @Then("bin should not be updated and proper error message with status {int} should be displayed {string}")
    public void bin_should_not_be_updated_and_proper_error_message_with_status_should_be_displayed(int status, String message) {
        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(ErrorMessage.class).getMessage(), message);
        scenario.log("Bin with id " + getScenarioContext().getContext(Context.ID) + " could not be updated because " + message);
    }

    @When("I try to update bin")
    public void i_try_to_update_bin() {
        response = getEndPoints().updateBin(binId);
        scenario.log("PUT API is called to get update bin details");
    }
}
