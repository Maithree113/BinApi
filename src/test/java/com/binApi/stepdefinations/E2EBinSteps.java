package com.binApi.stepdefinations;

import com.binApi.utilities.APIUtils.TestContext;
import com.pojo.deleteBinVersion.DeleteBinResponse;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Then;
import org.testng.Assert;

public class E2EBinSteps extends BaseStep {

    public E2EBinSteps(TestContext testContext) {
        super(testContext);
    }

    @Before
    public void beforeStep(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("bin should get deleted with status {int} and message {string}")
    public void bin_should_be_deleted(int status, String message) {
        Assert.assertEquals(response.getStatusCode(), status);
        Assert.assertEquals(response.getBody().as(DeleteBinResponse.class).getMetaData().getId(), binId);
        Assert.assertEquals(response.getBody().as(DeleteBinResponse.class).getMessage(), message);
        scenario.log("Bin with id " + binId + " " + message);
    }
}
