package com.binApi.stepdefinations;

import com.binApi.utilities.APIUtils.EndPoints;
import com.binApi.utilities.APIUtils.ScenarioContext;
import com.binApi.utilities.APIUtils.TestContext;
import io.cucumber.java.Scenario;
import io.restassured.response.Response;

public class BaseStep {

    private final ScenarioContext scenarioContext;
    protected Scenario scenario;
    protected static Response response;
    protected static String value = "true";
    protected static String requestBody = "Hello World";
    protected static String updateRequestBody = "Hello Quin";
    protected static String binCollection = "Sample Collection";
    protected static String binName = "Sample Bin";
    protected static String binId = null;
    protected static String collectionId = null;
    private EndPoints endPoints;

    public BaseStep(TestContext testContext)
    {
        endPoints = testContext.getEndPoints();
        scenarioContext = testContext.getScenarioContext();
    }

    public EndPoints getEndPoints() {
        return endPoints;
    }

    public ScenarioContext getScenarioContext() {
        return scenarioContext;
    }
}
